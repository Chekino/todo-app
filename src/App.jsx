import React from "react";
import TodoBar from "./components/todobar/TodoBar";
import AddTask from "./components/addtask/AddTask";
function App() {
  return (
    <div className="container">
      <TodoBar />
      <div className="">
        <AddTask />
      </div>
    </div>
  );
}

export default App;
