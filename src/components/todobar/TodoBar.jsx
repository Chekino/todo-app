import React, { useEffect } from "react";
import "./todobar.css";
import { useState } from "react";

const TodoBar = () => {
  const texts = [
    "Quels sont vos plants du jours ?",
    "Exploitez le pouvoir de la productivité!",
    "Restez organisé, gardez une longueur d'avance.",
  ];

  const [indexActuel, setIndexActuel] = useState(0);
  useEffect(() => {
    const interval = setInterval(() => {
      setIndexActuel((prevIndex) => (prevIndex + 1) % texts.length);
    }, 3000);
    return () => clearInterval(interval);
  }, []);
  return (
    <div className="p-4">
      <div className="flex">
        <img
          className=""
          src="src/assets/img/mdi_human-hello-variant.svg"
          alt=""
        />
        <p className="font-Lexend text-color">Bonjour et Bienvenue</p>
      </div>
      <div className="text-color">
        {texts.map((text, idx) => (
          <div
            key={idx}
            style={{ display: idx === indexActuel ? "block" : "none" }}
          >
            {text}
          </div>
        ))}
      </div>
    </div>
  );
};

export default TodoBar;
